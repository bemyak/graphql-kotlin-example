package ru.bemyak.test.graphql

import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.stereotype.Component

@Component
class BookResolver(private val referenceDao: ReferenceDao, private val authorDao: AuthorDao) : GraphQLResolver<Book> {
    fun references(book: Book) =
            referenceDao.getRelationshipsForBook(book.id)
    fun author(book: Book) =
            authorDao.getAuthorById(book.authorId)
}
