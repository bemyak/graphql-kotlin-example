package ru.bemyak.test.graphql

import org.springframework.stereotype.Component

@Component
class AuthorDao {
    private val data = mutableListOf(
            Author(id = "0", name = "H.P. Lovecraft"),
            Author(id = "1", name = "Y. Aleynikov"),
            Author(id = "2", name = "J.K. Rowling"),
            Author(id = "3", name = "L. Carrol")
    )

    fun getAuthorById(id: String) = data.firstOrNull { book -> book.id == id }

    fun addAuthor(name: String): Author {
        val author = Author(id = data.size.toString(), name = name)
        data.add(author)
        return author
    }
}
