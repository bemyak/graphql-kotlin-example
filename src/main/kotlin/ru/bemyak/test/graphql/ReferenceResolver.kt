package ru.bemyak.test.graphql

import com.coxautodev.graphql.tools.GraphQLResolver
import org.springframework.stereotype.Component

@Component
class ReferenceResolver(private val bookDao: BookDao) : GraphQLResolver<Reference> {
    fun getFrom(reference: Reference) = bookDao.getBookById(reference.from)
    fun getTo(reference: Reference) = bookDao.getBookById(reference.to)
}
