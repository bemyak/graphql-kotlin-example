package ru.bemyak.test.graphql

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component

@Component
class BookMutationResolver(private val bookDao: BookDao) : GraphQLMutationResolver {
    fun createBook(name: String, authorId: String) = bookDao.createBook(name, authorId)
}
