package ru.bemyak.test.graphql

data class Book(
        val id: String,
        val name: String,
        val authorId: String
)
