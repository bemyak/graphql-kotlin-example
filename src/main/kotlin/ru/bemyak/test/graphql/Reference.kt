package ru.bemyak.test.graphql

data class Reference(
        val from: String,
        val to: String
)
