package ru.bemyak.test.graphql

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component

@Component
class AuthorMutationResolver(private val authorDao: AuthorDao) : GraphQLMutationResolver {
    fun createAuthor(name: String) = authorDao.addAuthor(name)
}
