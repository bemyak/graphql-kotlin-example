package ru.bemyak.test.graphql

import org.springframework.stereotype.Component

@Component
class BookDao {
    private val data = mutableListOf(
            Book(id = "0", name = "Call of Cthulhu", authorId = "0"),
            Book(id = "1", name = "Best memes of 2018", authorId = "1"),
            Book(id = "2", name = "Harry Potter", authorId = "2"),
            Book(id = "3", name = "Alice in Wonderland", authorId = "3")
    )

    fun getBookById(id: String) = data.firstOrNull { book -> book.id == id }
    fun getBooksByAuthor(authorId: String) = data.filter { it.authorId == authorId }

    fun createBook(name: String, authorId: String): Book {
        val book = Book(id = data.size.toString(), name = name, authorId = authorId)
        data.add(book)
        return book
    }

    fun getAllBooks(): List<Book> {
        return data
    }
}
