package ru.bemyak.test.graphql

import org.springframework.stereotype.Component

@Component
class ReferenceDao {
    private val data = mutableListOf(
            Reference(from = "1", to = "0"),
            Reference(from = "1", to = "2")
    )

    fun getRelationshipsForBook(bookId: String) = data.filter { relationship -> relationship.from == bookId }
}
