package ru.bemyak.test.graphql

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class LibraryApp

fun main(args: Array<String>) {
    SpringApplication.run(LibraryApp::class.java, *args)
}
