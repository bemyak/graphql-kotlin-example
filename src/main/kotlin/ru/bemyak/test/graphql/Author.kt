package ru.bemyak.test.graphql

data class Author(
        val id: String?, val name: String
)