package ru.bemyak.test.graphql

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component

@Component
class BookQueryResolver(
        private val bookDao : BookDao
) : GraphQLQueryResolver {
    fun getBookById(id: String) = bookDao.getBookById(id)
    fun getBooks() = bookDao.getAllBooks()
}
